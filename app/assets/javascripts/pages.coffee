# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
			$('.footer-opts').mouseenter ->
				$(this).css('letter-spacing', "4px")
				$('.words').fadeOut(1000)
				$('#home-Icon').fadeOut(1000)
				$('#home-login').fadeIn(2500)
				$('.user-access').fadeIn(2500)
				$('.home-pg').fadeOut()
				$('#home-thoughts').animate { "width" : "0%"
				}, 3500 
				return

			$('.mantra-words').click ->
				$('#charge').fadeIn(1500)
				$('#charge').fadeOut(7000)
				$('#mantra').val(@id)

			$('#about').click ->
				$('#med-tips').fadeIn(500)
				$(this).fadeOut()
				$('#med-tips').fadeOut(40000)
				$(this).fadeIn(45000)

			$('#med-tips').click ->
				$(this).fadeOut()
				$('#about').fadeIn(500)


			$('.mantra-words').mouseenter ->
				$(this).css("transform", "scale(1.3)")
				p = "mantra_"+ @id
				$('#'+ p).fadeIn()
				$('#' + p).fadeOut(3500)

			$('.mantra-words').mouseleave ->
				$(this).css("transform", "scale(1.0)")

			$('#charge').click ->
				$(this).fadeOut(500)

			$('#works').click ->
				$('#attributes').fadeIn(500)
				$('#attributes').fadeOut(40000)
				

			$('#attributes').click ->
				$(this).fadeOut()
	

			phrase = document.getElementById('bg_mantra').innerHTML
		
			c = parseInt( document.getElementById('set_counter').innerHTML) 
			i = 0
			pcode = 0
			pos = 0
			tCode = 0
			counter = c


			$('#type_mantra').focus ->
				$('#bg_mantra').css("opacity", "0.10")
				$(this).val(null)

			$('#type_mantra').keyup -> 
				if counter < 250
					a = $(this).val()
					if a[0] == ' '
						$(this).val(null)
						a = $(this).val()
					ogString = phrase.substr(0, a.length)
					tyString = a.substr(0, a.length)
					if tyString.toLowerCase() == ogString.toLowerCase()
						$(this).css("color", "#5C99B9")
						if tyString.length == phrase.length
							counter += 1
							document.getElementById('counter').innerHTML = counter + '/ 250'
							$(this).val(null)
							$('#tracker').val(counter)
							$('#counter_form').submit()
					else
						$(this).css("color", "rgba(128, 0, 0, 0.35")
				

			$('.footer-opts').mouseleave ->
				$(this).css('letter-spacing', '1px')

			$('#homeQuickAccess').click ->
				$('.home-access').fadeOut(500)
				$('#qa').fadeIn(1500)
				
			$('#homeSignUp').click ->
				$('.home-access').fadeOut(500)
				$('#sign-up').fadeIn(1500)

			$('#homeLogIn').click ->
				$('.home-access').fadeOut(500)
				$('#login').fadeIn(1500)