class Teacher < ApplicationRecord
	before_save { self.yogi = yogi.downcase }
	validates :yogi, presence: true, length: {maximum: 50}

	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }


    def Teacher.digest(string)
    	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
  	end
end
