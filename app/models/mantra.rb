class Mantra < ApplicationRecord
	validates :mantra, presence: true
	validates :phrase, presence: true
end
