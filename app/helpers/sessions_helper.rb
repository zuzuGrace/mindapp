module SessionsHelper
  def create_guest_session
    session[:expires_at] = Time.now + 55 * 60
    session[:status] = "starting"
    session[:counter] = 0
  end

  #ends the user session after specified time
  def end_access
  	@time = session[:expires_at]
  end

  def current_session?
    @time.nil?
  end

  def time_split
  	sp = end_access.split("T")
  	sp[1].split(".")[0]
  end

  def active_mantra(mantra)
    session[:mantra_id] = mantra
  end

  def get_mantra 
    @m = session[:mantra_id]
  end

  def log_in(yogi)
    session[:teacher_id] = yogi.id
  end

  def current_yogi
    if session[:teacher_id]
      @teacher ||= Teacher.find_by(id: session[:teacher_id])
    end
  end

  def logged_in?
    !current_yogi.nil?
  end

  def log_out
    session.delete(:teacher_id)
    @teacher = nil
  end


  def get_tracker
    @begin = session[:counter]
  end

def set_current_counter ( curr_counter )
  session[:current_counter] = curr_counter
end

def get_current_counter
  @count = session[:current_counter]
end

def session_status
  @status = session[:status]
end



end
