class ChargesController < ApplicationController
	def create
		@amount = 299

		 customer = Stripe::Customer.create({
		 	source: params[:stripeToken],
		 })
		 am = params[:mantra]
		 #pass this to the sessions helper 
		active_mantra(am)


		 @charge = Stripe::Charge.create({
		 	customer: customer.id, 
		 	amount: @amount, 
		 	description: "thatLovelyPracticeMeditation",
		 	currency: 'usd',
		 })

		 if !@charge.nil?
		 	redirect_to begin_path
		 end


		rescue Stripe::CardError => e
			flash[:error] = e.message
			redirect_to root_url
		end 



end
