class SessionsController < ApplicationController
	def create
		create_guest_session
		redirect_to meditation_path
	end

	def teacher_login
	end

	def connect
		yogi = Teacher.find_by(yogi: params[:session][:yogi].downcase)
		if yogi && yogi.authenticate(params[:session][:password])
			log_in yogi
			redirect_to yogi
		else
			flash.now[:error] = "Invalid LogIn"
			render 'teacher_login'
		end
	end

	def update
		session[:counter] = params[:session_update][:counter]
	end

	def destroy
		log_out
		redirect_to root_url
	end
end
