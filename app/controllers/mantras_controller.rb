class MantrasController < ApplicationController
	before_action :end_guest_session, only: [:meditation]

	def index
		@mantra = Mantra.all
	end 


	def meditation
		@mantra = Mantra.all
	end


	def new
		@mantra = Mantra.new
	end

	def create
		@mantra = Mantra.new(mantra_params)
		if @mantra.save 
			flash[:success] = "Created the Mantra"
		else
			flash[:error] = "Unable to create mantra"
		end
	end

	def show
		@mantra = Mantra.find(params[:id])
	end

	def edit
		@edit_mantra = Mantra.find(params[:id])
	end

	def update
		@mantra = Mantra.find(params[:id])
		if @mantra.update(mantra_params)
			flash[:success] = "Updated the mantra"
		else 
			flash[:error] = "Unable to update"
		end
	end

	def destroy
	end

	private
	def mantra_params
		params.require(:mantra).permit(:mantra, :phrase)
	end

	def end_guest_session
			if  end_access < Time.now
				session[:status] = nil
				session[:counter] = nil
				 session[:expires_at] = nil 
				redirect_to root_url
			else
				if session_status == "starting"
					session[:status] = "in action"
				else
					session[:counter] = get_tracker
				end
			end
	end

end
