class TeachersController < ApplicationController
	def show
		@yogi = Teacher.find(params[:id])
		@mantras = Mantra.all
	end

	def create
		@yogi = Teacher.new(yogi_params)
		if @yogi.save 
			flash[:success] = "YOGI added"
			
		else 
			flash[:error] = "YOGI not added."
			redirect_to @yogi
		end
	end

	def edit
		@yogi = Teacher.find(params[:id])
	end

	def update
		@yogi = Teacher.find(params[:id])
		if @yogi.update_attributes(yogi_params)
			flash[:success] = "Updated"
			redirect_to @yogi
		else
			flash[:error] = "Unable to Update"
		end
	end

	def destroy
		@yogi = Teacher.find(params[:id])
		@yogi.destroy
	end

	private
	 	def yogi_params
	 		params.require(:yogi).permit(:yogi, :password, :password_confirmation)
	 	end
end
