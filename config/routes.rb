Rails.application.routes.draw do
  root 'pages#home'
  get '/meditate', to: 'users#show'

  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/teacher_login', to: 'sessions#teacher_login'
  post '/teacher_login', to: 'sessions#connect'
  delete '/teacher_logout', to: 'sessions#destroy'

 post '/update_counter', to: 'sessions#update'

  post '/charge', to: 'charges#create'

  get '/begin', to: 'sessions#begin'
  get '/meditation', to: 'mantras#meditation'

  get '/add', to: 'teachers#new'



  post '/add_mantra', to: 'mantras#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :mantras
  resources :teachers
end
