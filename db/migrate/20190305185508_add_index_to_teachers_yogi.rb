class AddIndexToTeachersYogi < ActiveRecord::Migration[5.2]
  def change
  	add_index :teachers, :yogi, unique: true
  end
end
