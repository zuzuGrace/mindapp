class CreateMantras < ActiveRecord::Migration[5.2]
  def change
    create_table :mantras do |t|
      t.string :mantra
      t.string :phrase

      t.timestamps
    end
  end
end
